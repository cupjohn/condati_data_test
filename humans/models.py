from functools import reduce

from faker import Faker
from pyrsistent import pmap, thaw
from sqlalchemy import BigInteger, Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()
metadata = Base.metadata

fake = Faker()


class DataSeed:
    @classmethod
    def get_parents(cls):
        # StackOverflow search to figure out how to get the class names of the relationships
        # this pulls from the `relationships` defined on the classc
        return [r.mapper.class_.__name__ for r in cls.__mapper__.relationships]

    @classmethod
    def delete_(cls, session):
        schema = ''
        if '__table_args__' in cls.__dict__:
            table_args = cls.__table_args__
            if isinstance(table_args, tuple):
                table_args = table_args[1]
            schema = f'{table_args["schema"]}.'

        session.execute('TRUNCATE {}{} CASCADE;'.format(schema, cls.__tablename__))
        session.commit()

    @classmethod
    def seed(cls, session):
        parents = cls.get_parents()
        if parents is not None:
            data_set = list(map(lambda x: eval(x).seed(session), parents))
            data_set = reduce(lambda x, y: x + pmap(y), data_set, pmap())
            # flush to get the db_id set, rather than commit
            # gives the user the option to not store (commit) the data or rollback
            session.flush()
            return data_set
        return None

    @classmethod
    def insert_data(cls, session, data_set, new_data):
        """
        Creates a new class object and adds it to the session.  It returns the data_set with the new object added.
        :param session: session object that all of it builds on
        :param data_set: pmap object of the set of table objects
        :param new_data: dictionary of the data to be created as an object.
        :return: pmap object of the full set of objects with the new table object added.  The key of the key value is the table name.
        """
        new_class = cls(**new_data)
        data_set = data_set.set(cls.__tablename__, new_class)
        session.add(new_class)

        return data_set


class World(Base, DataSeed):
    __tablename__ = 'world'

    id = Column(BigInteger, primary_key=True)
    name = Column(String(255))
    size = Column(Integer)

    @classmethod
    def seed(cls, session, data_seed=None):
        if data_seed is None:
            data_seed = {'name': fake.name(), 'size': fake.numerify('###')}

        data_set = cls.insert_data(session, pmap(), data_seed)

        return thaw(data_set)


class Tribe(Base, DataSeed):
    __tablename__ = 'tribe'

    id = Column(BigInteger, primary_key=True)
    hair = Column(String(255))
    type = Column(String(255))
    world_id = Column(ForeignKey('world.id'))

    world = relationship('World')

    @classmethod
    def seed(cls, session, data_seed=None):
        data_set = super().seed(session)

        tribe_data = {'world_id': data_set['world'].id}

        if data_seed is None:
            data_seed = {'hair': fake.name(), 'type': fake.name()}

        tribe_data.update(data_seed)

        data_set = cls.insert_data(session, data_set, tribe_data)

        return thaw(data_set)


class Human(Base, DataSeed):
    __tablename__ = 'human'

    id = Column(BigInteger, primary_key=True)
    first_name = Column(String(255))
    last_name = Column(String(255))
    age = Column(Integer)
    tribe_id = Column(ForeignKey('tribe.id'))

    tribe = relationship('Tribe')

    @classmethod
    def seed(cls, session, data_seed=None):
        data_set = super().seed(session)

        human_data = {'tribe_id': data_set['tribe'].id}

        if data_seed is None:
            data_seed = {'first_name': fake.name(), 'last_name': fake.name(), 'age': fake.numerify('##')}

        human_data.update(data_seed)

        data_set = cls.insert_data(session, data_set, human_data)

        return thaw(data_set)
