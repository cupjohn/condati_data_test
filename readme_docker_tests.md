# Docker for Tests
- build the image
    - `docker-compose -f <yaml-file-name> build --no-cache python`
- shell into the image & run tests
    - `docker-compose -f <yaml-file-name> run python sh`
    - `make test`
    
## make file
- `make test` : runs all tests
- `make test-small one=<>` : runs tests that are passed in the `one` parameter
